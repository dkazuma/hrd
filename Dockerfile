FROM node:12.16
# Create app directory
WORKDIR /usr/src/app
# Install app dependenciesf
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY * ./

RUN npm install
RUN npm audit fix

# If you are building your code for production
# RUN npm install --only=production# Bundle app source

COPY . .
EXPOSE 8080

CMD [ "npm", "start" ]