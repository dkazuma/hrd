/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  Label,
  CardBody,
  FormGroup,
  Input
} from "reactstrap";

import ExamplesNavbar from "components/Navbars/AuthNavbar.js";
import swal from 'sweetalert';

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      identitas_user: '',
      no_identitas: '',
      alamat_user: '',
      no_telp: '',
      fax: '',
      web_address: '',
      email_pendaftaran: '',
      nama_pic: '',
      alamat_pic: '',
      no_telp_pic: '',
      fax_pic: '',
      email_pic: '',
      username: '',
      password: '',
      konfirmasi_password: '',
      nama_perusahaan: '',
      jenis_user: '',
    }
  }

  handleValidation(name) {
    return function (event) {
      this.setState({ [name]: event.target.value })
    }
  }

  check() {
    var payload = {
      username: this.state.username,
      password: this.state.password,
      email: this.state.email_pendaftaran,
      fullname: this.state.nama_perusahaan,
      jenis_user_group: "1",
      jenis_user: this.state.jenis_user,
      jenis_identitas: this.state.identitas_user,
      no_identitas: this.state.no_identitas,
      status_user: "0"
    }
    console.log(JSON.stringify(payload));
    fetch('http://10.8.3.198:8000/v1/pabean', {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    })
      .then(res => res.json())
      .then((data) => {
        this.setState({ contacts: JSON.stringify(payload) })
      })
      .catch(console.log)
    swal({
      title: "Registration succeed!",
      text: "",
      icon: "success",
      button: "OK",
    });
  }


  render() {
    return (
      <>
                <ExamplesNavbar />
                <div
                    className="page-header"
                    // style={{
                    //     backgroundImage: "url(" + require("assets/img/theme/register-page-background.jpg") + ")"
                    // }}
                >
                    <Card className="text-center parent-card-custom">
                        <CardBody>
                            <label className="title-custom">FORM ISIAN DATA PENDAFTARAN USER</label>
                            <form>
                                <Card>
                                    <CardBody>
                                        <FormGroup className="text-left">
                                            <Label className="title-custom">DATA IDENTITAS USER</Label>
                                        </FormGroup>
                                        <FormGroup className="text-left">
                                            <Label for="nama-perusahaan">Nama Perusahaan</Label>
                                            <Input type="text" id="nama-perusahaan" onChange={this.handleValidation("nama_perusahaan").bind(this)} placeholder="" />
                                        </FormGroup>
                                        <FormGroup className="text-left">
                                            <Label for="jenis-user">Jenis User</Label>
                                            <Input type="select" name="jenis-user" id="jenis-user" onChange={this.handleValidation("jenis_user").bind(this)}>
                                                <option value="">Pilih Jenis User</option>
                                                <option value="1">Perusahaan (PT, Persero, CV, Dll)</option>
                                                <option value="2">Instansi Pemerintah</option>
                                                <option value="3">Organisasi</option>
                                                <option value="4">Perseorangan</option>
                                            </Input>
                                        </FormGroup>
                                        <FormGroup className="text-left">
                                            <Label>Jika Anda adalah perusahaan isikan identitas dengan nomor NPWP 15 digit untuk keperluan Anda melakukan. Untuk user selain perusahaan seperti orang/instansi bisa menggunakan identitas lain seperti KTP / SIM / Lainnya !</Label>
                                        </FormGroup>
                                        <div className="form-row text-left">
                                            <FormGroup className="col-md-6">
                                                <Label for="identitas_user">Jenis Identitas</Label>
                                                <Input type="select" name="identitas_user" id="identitas_user" onChange={this.handleValidation("identitas_user").bind(this)}>
                                                    <option value="">Pilih</option>
                                                    <option value="1">NPWP 15 Digit</option>
                                                    <option value="2">KTP</option>
                                                    <option value="3">SIM</option>
                                                    <option value="4">Pasport</option>
                                                    <option value="5">Lainnya</option>
                                                </Input>
                                            </FormGroup>
                                            <FormGroup className="col-md-6" >
                                                <Label for="no-identitas">No. Identitas</Label>
                                                <Input type="text" id="no-identitas" name="no-identitas" onChange={this.handleValidation("no_identitas").bind(this)} />
                                            </FormGroup>
                                        </div>
                                        <FormGroup className="text-left">
                                            <Label for="alamat_user">Alamat User</Label>
                                            <Input type="text" id="alamat_user" placeholder="" onChange={this.handleValidation("alamat_user").bind(this)} />
                                        </FormGroup>
                                        <div className="form-row text-left">
                                            <FormGroup className="col-md-6">
                                                <Label for="no-telpr">Telepon</Label>
                                                <Input type="number" name="no-telp" id="no-telp" onChange={this.handleValidation("no_telp").bind(this)} />
                                            </FormGroup>
                                            <FormGroup className="col-md-6" >
                                                <Label for="fax">Fax</Label>
                                                <Input type="number" id="fax" name="fax" onChange={this.handleValidation("fax").bind(this)} />
                                            </FormGroup>
                                        </div>
                                        <FormGroup className="text-left">
                                            <Label for="web-address">Alamat Website</Label>
                                            <Input type="text" id="web-address" placeholder="" onChange={this.handleValidation("web_address").bind(this)} />
                                        </FormGroup>
                                        <FormGroup className="text-left">
                                            <Label>Alamat email pendaftaran harus valid, dapat diakses dan tidak memfilter pengiriman link kode aktivasi beserta attachment pdf yang berisi username dan password dan nomor pin. Isikan dengan huruf kecil semua.</Label>
                                        </FormGroup>
                                        <FormGroup className="text-left">
                                            <Label for="email-pendaftaran">Alamat Email Pendaftaran</Label>
                                            <Input type="text" id="email-pendaftaran" placeholder="" onChange={this.handleValidation("email_pendaftaran").bind(this)} />
                                        </FormGroup>
                                    </CardBody>
                                </Card>
                                <Card>
                                    <CardBody>
                                        <FormGroup className="text-left">
                                            <Label className="title-custom">DATA YANG MELAKUKAN PENDAFTARAN</Label>
                                        </FormGroup>
                                        <FormGroup className="text-left">
                                            <Label for="nama-pic">Nama PIC</Label>
                                            <Input type="text" id="nama-pic" placeholder="" onChange={this.handleValidation("nama_pic").bind(this)} />
                                        </FormGroup>
                                        <FormGroup className="text-left">
                                            <Label for="alamat-pic">Alamat PIC</Label>
                                            <Input type="text" id="alamat-pic" placeholder="" onChange={this.handleValidation("alamat_pic").bind(this)} />
                                        </FormGroup>
                                        <div className="form-row text-left">
                                            <FormGroup className="col-md-6">
                                                <Label for="no-telp-pic">Telepon</Label>
                                                <Input type="number" name="no-telp-pic" id="no-telp-pic" onChange={this.handleValidation("no_telp_pic").bind(this)} />
                                            </FormGroup>
                                            <FormGroup className="col-md-6" >
                                                <Label for="no-identitas">Fax</Label>
                                                <Input type="number" id="fax-pic" name="fax-pic" onChange={this.handleValidation("fax_pic").bind(this)} />
                                            </FormGroup>
                                        </div>
                                        <FormGroup className="text-left">
                                            <Label for="email-pic">Alamat Email PIC</Label>
                                            <Input type="email" id="email-pic" placeholder="" onChange={this.handleValidation("email_pic").bind(this)} />
                                        </FormGroup>
                                    </CardBody>
                                </Card>
                                <Card>
                                    <CardBody>
                                        <FormGroup className="text-left">
                                            <Label className="title-custom">DATA USERNAME & PASSWORD</Label>
                                        </FormGroup>
                                        <FormGroup className="text-left">
                                            <Label for="username">Username</Label>
                                            <Input type="text" id="username" placeholder="minimal 6 - 12 karakter (tanpa spasi)" onChange={this.handleValidation("username").bind(this)} />
                                        </FormGroup>
                                        <FormGroup className="text-left">
                                            <Label for="password">Password</Label>
                                            <Input type="password" id="password" placeholder="" onChange={this.handleValidation("password").bind(this)} />
                                        </FormGroup>
                                        <FormGroup className="text-left">
                                            <Label for="konfirmasi-password">Konfirmasi Password</Label>
                                            <Input type="password" id="konfirmasi-password" placeholder="" onChange={this.handleValidation("konfirmasi_password").bind(this)} />
                                        </FormGroup>
                                    </CardBody>
                                </Card>
                                <FormGroup className="text-left">
                                    <Label>Setelah mengisi isian data di atas, periksa kembali untuk memastikan data-datanya benar. Kemudian klik tombol 'SAVE' berikut ini untuk mengirimkan data pendaftaran user. Segera setelah data di submit, sistem kami akan mengirimkan email berisi kode aktivasi user ke alamat email pendaftaran yang anda isikan di atas</Label>
                                </FormGroup>
                                {/* <div className="form-row text-left">
                                    <FormGroup className="col-md-6">
                                        <Label for="inputCity">City</Label>
                                        <Input type="text" id="inputCity" />
                                    </FormGroup>
                                    <FormGroup className="col-md-4">
                                        <Label for="inputState">State</Label>
                                        <Input type="select" name="select" id="inputState" >
                                            <option>Choose...</option>
                                            <option>...</option>
                                        </Input>
                                    </FormGroup>
                                    <FormGroup className="col-md-2">
                                        <Label for="inputZip">Zip</Label>
                                        <Input type="text" id="inputZip" />
                                    </FormGroup>
                                </div> */}
                                <FormGroup check>
                                    <Label className="form-check-label">
                                        <Input className="form-check-input" type="checkbox" value="" />
                                        Check me out
                                        <span className="form-check-sign">
                                            <span className="check"></span>
                                        </span>
                                    </Label>
                                </FormGroup>
                                <Button className="btn-round btn-icon" color="primary" onClick={this.check.bind(this)} style={{ margin: "10px" }} ><i className="fa fa-save" /> Save</Button>
                                <Button type="submit" className="btn-round btn-icon" color="warning" style={{ margin: "10px" }}><i className="fa fa-undo" />Reset</Button>
                                <Button type="submit" className="btn-round btn-icon" color="danger" style={{ margin: "10px" }}><i className="fa fa-arrow-left" />Kembali</Button>
                            </form>
                            {/* <div>
                                <button onClick={() => this.setState({ show: true })}>Alert</button>
                                <SweetAlert
                                    show={this.state.show}
                                    title="Demo"
                                    text="SweetAlert in React"
                                    onConfirm={() => this.setState({ show: false })}
                                />
                            </div> */}
                        </CardBody>
                    </Card>
                </div>

            </>

    );
  }
}

export default Register;
