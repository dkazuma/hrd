/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// reactstrap components
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col,
} from "reactstrap";
// core components

import Header from "components/Headers/Header.js";
import axios from "axios";

class UserFrom extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {},

            eksportir_nama: "",
            eksportir_alamat: "",
            eksportir_niper: "",
            eksportir_status: "",
            penerima_nama: "",
            penerima_alamat: "",
            penerima_negara: "",
            ppjk_nama: "",
            pembeli_nama: ""
        }
    }

    componentDidMount() {
      this.getUser();
    }

    getUser = () => {
        const token = localStorage.getItem("token");
        if (!token) {
          this.props.history.push("/auth/login");
        }
        axios.get("http://localhost:3000/users/me", {
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        }).then(resGetUser => {
            if (resGetUser.status === 200) {
            this.setState({ user: resGetUser.data });
          }
        }).catch(err => {
          if (err.response && err.response.status === 401) {
            localStorage.clear();
            this.props.history.push("/auth/login");
          }
          console.log(err)
        });
    }

    submit = (e) => {
        e.preventDefault();

        const token = localStorage.getItem("token");
        
        axios.post("http://10.8.3.198:3000/peb", {
            pengangkutan_sarana: "",
            ppjk_nama: this.state.ppjk_nama,
            penerima_nama: this.state.penerima_nama,
            pembeli_nama: this.state.pembeli_nama,
            pelabuhan_tempat_muat: "",
            pelengkap_invoice: "",
            pemeriksaan_lokasi: "",
            penyerahan_cara: "",
            transaksi_freight: "",
            peti_kemas_nomor: "",
            ekspor_berat_kotor: "",
        }, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        }).then(res => {
            if (res.status === 201) {
                this.props.history.push("/admin/tables");
            }
        }).catch(err => {
          if (err.response && err.response.status === 401) {
            localStorage.clear();
            this.props.history.push("/auth/login");
          }
          console.log(err)
        });;
    }

    render() {
        return (
            <>
                <Header />
                {/* Page content */}
                <Container className="mt--7" fluid>
                    <Row>
                        <Col className="order-xl-1">
                            <Card className="bg-secondary shadow">
                                <Form role="form" onSubmit={this.submit}>
                                <CardHeader className="bg-white border-0">
                                    <Row className="align-items-center">
                                        <Col xs="10">
                                            <h3 className="mb-0">Submission Form</h3>
                                        </Col>
                                        <Col xs="1">
                                            <Button
                                                type="submit"
                                                color="primary"
                                                size="md"
                                            >Submit</Button>
                                        </Col>
                                        <Col xs="1">
                                            <Button
                                                color="secondary"
                                                href="#"
                                                onClick={e => {
                                                    e.preventDefault();
                                                    this.props.history.goBack();
                                                }}
                                                size="md"
                                            >Batal</Button>
                                        </Col>
                                    </Row>
                                </CardHeader>
                                <CardBody>
                                        <h6 className="heading-small text-muted mb-4">Eksportir</h6>
                                        <div className="pl-lg-4">
                                            <Row>
                                                <Col lg="6">
                                                    <FormGroup>
                                                        <label
                                                            className="form-control-label"
                                                            htmlFor="input-eksportir-nama"
                                                        >
                                                            Nama
                            </label>
                                                        <Input
                                                            className="form-control-alternative"
                                                            defaultValue=""
                                                            id="input-eksportir-nama"
                                                            placeholder="Budi Setiawan"
                                                            type="text"
                                                            onChange={(e) => this.setState({ eksportir_nama: e.target.value })}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col lg="6">
                                                    <FormGroup>
                                                        <label
                                                            className="form-control-label"
                                                            htmlFor="input-eksportir-alamat"
                                                        >
                                                            Alamat
                            </label>
                                                        <Input
                                                            className="form-control-alternative"
                                                            id="input-eksportir-alamat"
                                                            placeholder="Jalan Kemenangan No. 99"
                                                            type="test"
                                                            onChange={(e) => this.setState({ eksportir_alamat: e.target.value })}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col lg="6">
                                                    <FormGroup>
                                                        <label
                                                            className="form-control-label"
                                                            htmlFor="input-eksportir-niper"
                                                        >
                                                            NIPER
                            </label>
                                                        <Input
                                                            className="form-control-alternative"
                                                            defaultValue=""
                                                            id="input-eksportir-niper"
                                                            placeholder="1234"
                                                            type="text"
                                                            onChange={(e) => this.setState({ eksportir_niper: e.target.value })}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col lg="6">
                                                    <FormGroup>
                                                        <label
                                                            className="form-control-label"
                                                            htmlFor="input-eksportir-status"
                                                        >
                                                            Status
                            </label>
                                                        <Input
                                                            className="form-control-alternative"
                                                            defaultValue=""
                                                            id="input-eksportir-status"
                                                            placeholder="-"
                                                            type="text"
                                                            onChange={(e) => this.setState({ eksportir_status: e.target.value })}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                        </div>
                                        <hr className="my-4" />
                                        <h6 className="heading-small text-muted mb-4">Penerima</h6>
                                        <div className="pl-lg-4">
                                            <Row>
                                                <Col lg="4">
                                                    <FormGroup>
                                                        <label
                                                            className="form-control-label"
                                                            htmlFor="input-penerima-nama"
                                                        >Nama</label>
                                                        <Input
                                                            className="form-control-alternative"
                                                            defaultValue=""
                                                            id="input-penerima-nama"
                                                            placeholder="-"
                                                            type="text"
                                                            onChange={(e) => this.setState({ penerima_nama: e.target.value })}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col lg="4">
                                                    <FormGroup>
                                                        <label
                                                            className="form-control-label"
                                                            htmlFor="input-penerima-alamat"
                                                        >Alamat</label>
                                                        <Input
                                                            className="form-control-alternative"
                                                            defaultValue=""
                                                            id="input-penerima-alamat"
                                                            placeholder="-"
                                                            type="text"
                                                            onChange={(e) => this.setState({ penerima_alamat: e.target.value })}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col lg="4">
                                                    <FormGroup>
                                                        <label
                                                            className="form-control-label"
                                                            htmlFor="input-negara"
                                                        >Negara</label>
                                                        <Input
                                                            className="form-control-alternative"
                                                            id="input-negara"
                                                            placeholder="-"
                                                            type="text"
                                                            onChange={(e) => this.setState({ penerima_negara: e.target.value })}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                        </div>
                                        <hr className="my-4" />
                                        {/* Description */}
                                        <h6 className="heading-small text-muted mb-4">PPJK</h6>
                                        <div className="pl-lg-4">
                                            <Row>
                                                <Col lg="12">
                                                    <FormGroup>
                                                        <label
                                                            className="form-control-label"
                                                            htmlFor="input-ppjk-nama"
                                                        >Nama</label>
                                                        <Input
                                                            className="form-control-alternative"
                                                            defaultValue=""
                                                            id="input-ppjk-nama"
                                                            placeholder="-"
                                                            type="text"
                                                            onChange={(e) => this.setState({ ppjk_nama: e.target.value })}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                        </div>
                                        <hr className="my-4" />
                                        {/* Description */}
                                        <h6 className="heading-small text-muted mb-4">Pembeli</h6>
                                        <div className="pl-lg-4">
                                            <Row>
                                                <Col lg="12">
                                                    <FormGroup>
                                                        <label
                                                            className="form-control-label"
                                                            htmlFor="input-pembeli-nama"
                                                        >Nama</label>
                                                        <Input
                                                            className="form-control-alternative"
                                                            defaultValue=""
                                                            id="input-pembeli-nama"
                                                            placeholder="-"
                                                            type="text"
                                                            onChange={(e) => this.setState({ pembeli_nama: e.target.value })}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                        </div>
                                        <Row className="align-items-center">
                                            <Col xs="6">
                                                <Button
                                                    type="submit"
                                                    color="primary"
                                                    size="md"
                                                    className="float-right"
                                                >Submit</Button>
                                            </Col>
                                            <Col xs="6">
                                                <Button
                                                    color="secondary"
                                                    href="#"
                                                    onClick={e => {
                                                        e.preventDefault();
                                                        this.props.history.goBack();
                                                    }}
                                                    size="md"
                                                >Batal</Button>
                                            </Col>
                                        </Row>
                                    </CardBody>
                                </Form>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }
}

export default UserFrom;
