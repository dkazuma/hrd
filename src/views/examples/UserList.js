/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import classnames from "classnames";
import axios from "axios";
import moment from "moment";

// reactstrap components
import {
  Badge,
  Card,
  CardHeader,
  CardFooter,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Media,
  Pagination,
  PaginationItem,
  PaginationLink,
  Progress,
  Table,
  Container,
  Row,
  UncontrolledTooltip,
  NavItem,
  NavLink,
  Nav,
  Button
} from "reactstrap";
// core components
import Header from "components/Headers/Header.js";

class UserList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      user: {}
    }
  }

  deleteUser(id) {
    if (window.confirm("Anda Yakin?")) {
      var request = require('request');
      var options = {
        'method': 'DELETE',
        'url': 'http://10.8.3.198:8000/v1/pabean2',
        headers: {
          "Authorization": `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIzbTh5d0RVNHowSzJVTVNPeWRPNWVZaURnMEZYOG5ERyIsImlhdCI6MTUxNjIzOTAyMn0.i70S_qRLJkKgzA7o1YRkgcEZwTD9jL2b85uW58Ogkoo`
          
        },
        form: {
          'id': id
        }
      };
      request(options, function (error, response) {
        if (error) throw new Error(error);
        window.location.reload(false);
      });
    }
  }

  approveUser(id) {
    if (window.confirm("Anda Yakin?")) {
      var request = require('request');
      var options = {
        'method': 'PUT',
        'url': 'http://10.8.3.198:8000/v1/pabean2',
        headers: {
          "Authorization": `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIzbTh5d0RVNHowSzJVTVNPeWRPNWVZaURnMEZYOG5ERyIsImlhdCI6MTUxNjIzOTAyMn0.i70S_qRLJkKgzA7o1YRkgcEZwTD9jL2b85uW58Ogkoo`
          
        },
        form: {
          'id': id
        }
      };
      request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log(response.body);
      });
    }
  }

  componentDidMount() {
    this.getSubmissions();
  }

  // getSubmissions = () => {
  //   const token = localStorage.getItem("token");
  //   // if (!token) {
  //   //   this.props.history.push('/auth/login');
  //   // }

  //   const getItems = axios.get('http://10.8.3.198:8000/v1/pabean2', {
  //     headers: {
  //       "Content-Type": "application/json",
  //       "Authorization": `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIzbTh5d0RVNHowSzJVTVNPeWRPNWVZaURnMEZYOG5ERyIsImlhdCI6MTUxNjIzOTAyMn0.i70S_qRLJkKgzA7o1YRkgcEZwTD9jL2b85uW58Ogkoo`
        
  //     }
  //   });
  //   const getUser = axios.get('http://10.8.3.198:3000/users/me', {
  //     headers: {
  //       "Content-Type": "application/json",
  //       "Authorization": `Bearer ${token}`
  //     }
  //   });
  //   Promise.all([
  //     getItems,
  //     getUser
  //   ]).then(res => {
  //     const resGetItems = res[0];
  //     const resGetUser = res[1];
  //     if (resGetItems.status === 200) {
  //       const data = resGetItems.data;
  //       this.setState({ items: data });
  //     }

  //      if (resGetUser.status === 200) {
  //        this.setState({ user: resGetUser.data });
  //      }
  //   }).catch(err => {
  //     if (err.response && err.response.code === 401) {
  //       localStorage.clear();
  //        this.props.history.push('/auth/login');
  //     }
  //     console.log(err)
  //   });
  // }

  getSubmissions = () => {
    const headers = { 'Access-Control-Allow-Origin': '*', 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIzbTh5d0RVNHowSzJVTVNPeWRPNWVZaURnMEZYOG5ERyIsImlhdCI6MTUxNjIzOTAyMn0.i70S_qRLJkKgzA7o1YRkgcEZwTD9jL2b85uW58Ogkoo' }
    fetch('http://10.8.3.198:8000/v1/pabean2', { headers }).then(response => response.json()).then(
          (result) => {
            console.log(result);
            this.setState({
              items: result.data
            });
          },
          (error) => {
            this.setState({ error });
          }
        )
        // .then(response => response.json())
        // .then(data => this.setState({ totalReactPackages: data.total }));
  }
  //   const apiUrl = 'http://10.8.3.198:8000/v1/pabean2';
  //   fetch(apiUrl, {
  //     'method': 'GET', // or 'PUT'
  //     'headers': {
  //       'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIzbTh5d0RVNHowSzJVTVNPeWRPNWVZaURnMEZYOG5ERyIsImlhdCI6MTUxNjIzOTAyMn0.i70S_qRLJkKgzA7o1YRkgcEZwTD9jL2b85uW58Ogkoo'
  //     },
  //   }).then(res => res.json()).then(
  //     (result) => {
  //       console.log(result);
  //       this.setState({
  //         items: result.data
  //       });
  //     },
  //     (error) => {
  //       this.setState({ error });
  //     }
  //   )
  // }

  renderBadge = (status) => {
    switch (status) {
      case "1":
        return (
          <Badge color="" className="badge-dot mr-4">
            <i className="bg-success" />
              Approved
          </Badge>);
      case "0":
        return (
          <Badge color="" className="badge-dot mr-4">
            <i className="bg-danger" />
            Rejected
          </Badge>);
    }
  }

  render() {
    const { error, items } = this.state;
    var a = 1;
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
          {/* Dark table */}
          <Row>
            <div className="col">
              <Card className="bg-default shadow">
                <CardHeader className="bg-transparent border-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h6 className="text-uppercase text-light ls-1 mb-1">
                        Overview
                        </h6>
                      <h2 className="text-white mb-0">User List</h2>
                    </div>
                    <div className="col">
                      <Nav className="justify-content-end" pills>
                        <NavItem>
                          <NavLink
                            className={classnames("py-2 px-3", {
                              active: true
                            })}
                            href="#"
                            onClick={e => this.props.history.push('/admin/adduser')}
                          >
                            <span className="d-none d-md-block">Add</span>
                            <span className="d-md-none">Add</span>
                          </NavLink>
                        </NavItem>
                      </Nav>
                    </div>
                  </Row>
                </CardHeader>
                <Table
                  className="align-items-center table-dark table-flush"
                  responsive
                >
                  <thead className="thead-dark">
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Username</th>
                      <th scope="col">Nama (Nama Perusahaan)</th>
                      <th scope="col">Jenis User</th>
                      <th scope="col">Email</th>
                      <th scope="col">Nama PIC</th>
                      <th scope="col">Status</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {items.map((i) => {
                      return (
                        <tr>
                          <th><Media>
                            <span className="mb-0 text-sm">
                              {a++}
                            </span>
                          </Media></th>
                          <td>
                            <Media>
                              <span className="mb-0 text-sm">
                                {i.username}
                              </span>
                            </Media>
                          </td>
                          <td>
                            <Media>
                              <span className="mb-0 text-sm">
                                {i.fullname}
                              </span>
                            </Media>
                          </td>
                          <td>
                            <Media>
                              <span className="mb-0 text-sm">
                                {i.jenis_user}
                              </span>
                            </Media>
                          </td>
                          <td>
                            <Media>
                              <span className="mb-0 text-sm">
                                {i.email}
                              </span>
                            </Media>
                          </td>
                          <td>
                            <Media>
                              <span className="mb-0 text-sm">
                                {i.nama_pic}
                              </span>
                            </Media>
                          </td>
                          <td>{this.renderBadge(i.status_user)}</td>
                          <td>
                            <Button size="sm" color="success" fill onClick={() => this.approveUser(i.id)}>Approve</Button>
                    &nbsp;<Button size="sm" color="danger" fill onClick={() => this.deleteUser(i.id)}>Delete</Button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                </Table>
              </Card>
            </div>
          </Row>
        </Container>
      </>
    );
  }
}

export default UserList;
