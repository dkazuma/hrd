/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import classnames from "classnames";
import axios from "axios";
import moment from "moment";

// reactstrap components
import {
  Badge,
  Card,
  CardHeader,
  CardFooter,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Media,
  Pagination,
  PaginationItem,
  PaginationLink,
  Progress,
  Table,
  Container,
  Row,
  UncontrolledTooltip,
  NavItem,
  NavLink,
  Nav,
  Button
} from "reactstrap";
// core components
import Header from "components/Headers/Header.js";

class Tables extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      user: {}
    }
  }

  componentDidMount() {
    this.getSubmissions();
  }

  getSubmissions = () => {
    const token = localStorage.getItem("token");
    // if (!token) {
    //    this.props.history.push('/auth/login');
    // }

    const getItems = axios.get('http://10.8.3.198:3000/peb', {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${token}`
      }
    });
    const getUser = axios.get('http://10.8.3.198:3000/users/me', {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${token}`
      }
    });
    Promise.all([
      getItems,
      getUser
    ]).then(res => {
      const resGetItems = res[0];
      const resGetUser = res[1];

      if (resGetItems.status === 200) {
        const data = resGetItems.data;
        const sortedData = data.sort((a, b) => (moment(a.created_at).isAfter(moment(b.created_at)) ? -1 : 1));
        this.setState({ items: sortedData });
      }

      if (resGetUser.status === 200) {
        this.setState({ user: resGetUser.data });
      }
    }).catch(err => {
      if (err.response && err.response.status === 401) {
        localStorage.clear();
        // this.props.history.push('/auth/login');
      }
      console.log(err)
    });
  }

  renderBadge = (status) => {
    switch (status) {
      case "Waiting for Approval":
        return (
          <Badge color="" className="badge-dot mr-4">
            <i className="bg-warning" />
            Waiting for Approval
          </Badge>);
      case "Approved":
        return (
          <Badge color="" className="badge-dot mr-4">
            <i className="bg-success" />
              Approved
          </Badge>);
      case "Rejected":
        return (
          <Badge color="" className="badge-dot mr-4">
            <i className="bg-danger" />
            Rejected
          </Badge>);
    }
  }

  submitApproveReject = (nomor_pengajuan, type) => {
    const token = localStorage.getItem("token");
    if (!token) {
      this.props.history.push("/auth/login");
    }
    axios.post(`http://10.8.3.198:3000/peb/${nomor_pengajuan}/${type}`, {}, {
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
        }
    }).then(resGetUser => {
        if (resGetUser.status === 200) {
        this.setState({ user: resGetUser.data });
        this.props.history.push("/");
      }
    }).catch(err => {
      if (err.response && err.response.status === 401) {
        localStorage.clear();
        this.props.history.push("/");
        this.props.history.push("/auth/login");
      }
      console.log(err)
    });
  }

  render() {
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
          {/* Dark table */}
          <Row>
            <div className="col">
              <Card className="bg-default shadow">
                <CardHeader className="bg-transparent border-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h6 className="text-uppercase text-light ls-1 mb-1">
                        Overview
                        </h6>
                      <h2 className="text-white mb-0">Single Submission</h2>
                    </div>
                    <div className="col">
                      <Nav className="justify-content-end" pills>
                        <NavItem>
                          <NavLink
                            className={classnames("py-2 px-3", {
                              active: true
                            })}
                            href="#"
                            onClick={e => this.props.history.push('/admin/submissionform')}
                          >
                            <span className="d-none d-md-block">Add</span>
                            <span className="d-md-none">Add</span>
                          </NavLink>
                        </NavItem>
                      </Nav>
                    </div>
                  </Row>
                </CardHeader>
                <Table
                  className="align-items-center table-dark table-flush"
                  responsive
                >
                  <thead className="thead-dark">
                    <tr>
                      <th scope="col">Tanggal Submit</th>
                      <th scope="col">No. Pengajuan</th>
                      <th scope="col">Status</th>
                      <th scope="col">Action</th>
                      <th scope="col" />
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.items.map((i) => {
                      return (
                        <tr>
                          <th scope="row">{moment(i.created_at).format("DD MMMM YYYY HH:mm")}</th>
                          <td>
                            <Media>
                              <span className="mb-0 text-sm">
                                {i.nomor_pengajuan}
                              </span>
                            </Media>
                          </td>
                          <td>{this.renderBadge(i.status)}</td>
                          <td>
                            {/* <Button size="sm" color="info">Lihat Detail</Button> */}
                            {
                              i.status === "Waiting for Approval" && this.state.user.role === "bc" ? (
                                <>
                                  <Button size="sm" color="success" onClick={(e) => {
                                    this.submitApproveReject(i.nomor_pengajuan, "approve")
                                  }}>Approve</Button>
                                  <Button size="sm" color="danger" onClick={(e) => {
                                    this.submitApproveReject(i.nomor_pengajuan, "reject")
                                  }}>Reject</Button>
                                </>) : null
                            }
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                </Table>
              </Card>
            </div>
          </Row>
        </Container>
      </>
    );
  }
}

export default Tables;
