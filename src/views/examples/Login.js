/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import axios from "axios";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
  Alert
} from "reactstrap";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      alert: {
        visible: false,
        color: "default",
        message: ""
      }
    }
  }

  onSubmit = (e) => {
    
    e.preventDefault();
    const { username, password } = this.state
    localStorage.setItem("username", username);
    // this.props.history.push('/');
    axios.post('http://10.8.3.198:3000/users/login', {
      username,
      password
    }, {
      headers: {
        "Content-Type": "application/json",
      }
    }).then(res => {
      if (res.status === 200) {
        const token = res.data.token;
        localStorage.setItem("token", token);
        localStorage.setItem("username", username);
        this.setState({
          alert: {
            visible: true,
            color: "success",
            message: "Sukses login: Anda akan segera dialihkan ke halaman utama"
          }
        });
        this.props.history.push('/');
      }
    }).catch(err => {
      const message = err.response ? err.response.data.error : err.message;
      localStorage.setItem("username", username);
      // this.props.history.push('/');
      this.setState({
        alert: {
          visible: true,
          color: "danger",
          message: "Login gagal: " + message
        }
      });
    });
  }

  render() {
    return (
      <>
        <Col lg="5" md="7">
          <Card className="bg-secondary shadow border-0">
            <CardBody className="px-lg-5 py-lg-5">
              {
                this.state.alert.visible ? 
                (<Alert color={this.state.alert.color}>
                  {this.state.alert.message}
                </Alert>) : null
              }
              <div className="text-center text-muted mb-4">
                <small>Login dengan akun Anda</small>
              </div>
              <Form role="form" onSubmit={this.onSubmit}>
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-single-02" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Username"
                      type="text"
                      onChange={(e) => this.setState({ username: e.target.value })}
                      value={this.state.username}
                    />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Password"
                      type="password"
                      onChange={(e) => this.setState({ password: e.target.value })}
                      value={this.state.password}
                    />
                  </InputGroup>
                </FormGroup>
                <div className="custom-control custom-control-alternative custom-checkbox">
                  <input
                    className="custom-control-input"
                    id=" customCheckLogin"
                    type="checkbox"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor=" customCheckLogin"
                  >
                    <span className="text-muted">Remember me</span>
                  </label>
                </div>
                <div className="text-center">
                  <Button className="my-4" color="primary" type="submit">
                    Login
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
          <Row className="mt-3">
            <Col xs="6">
              <a
                className="text-light"
                href="#pablo"
                onClick={e => e.preventDefault()}
              >
                <small>Buat akun baru</small>
              </a>
            </Col>
          </Row>
        </Col>
      </>
    );
  }
}

export default Login;
